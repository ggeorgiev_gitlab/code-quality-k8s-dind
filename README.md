# Run code quality in OpenShift with GitLab Runner Operator (v1.12)


1. Install GitLab Runner Operator in your preferred manner

2. Create SA

```shell
oc create sa dind-sa
oc adm policy add-scc-to-user anyuid -z dind-sa -n dind-test
oc adm policy add-scc-to-user -z dind-sa privileged
```

3. Replace namespace in all files

4. Replace token in `gitlab-runner-secret.yml`

5. Create related resources:

```shell
oc apply -f gitlab-runner-secret.yml
oc create configmap custom-config-toml --from-file config.toml=template.config.toml
oc apply -f gitlab-runner.yml
```

6. Optionally create the PVC with `pvc.yml` and uncomment it in `template.config.toml`. It would allow to cache the pulled docker images. CodeQuality pulls some large images so adding cache could improve performance.

7. Use the job definition from `gitlab-ci.yml`.

8. Job log should be similar to `job.log`

## Notes

1. Make sure that the `vfs` storage driver is used
1. The dind service needs to serve at both a unix socket and tcp socket since we need the tcp socket to connect the docker client in the job itself and the unix socket to pass it down to the code quality image
1. Socket needs to be shared with a volume on `/var/run` to be accessible from the build container
1. Change namespaces from `ggeorgiev`!!!